#!/bin/bash -e

PWD=$(dirname $(readlink -f $0))
source "${PWD}/git-common.sh"

function list() {
  git stash list | cut -d : -f 3- | trim | while read line; do
    echo $line | tr '$' '\n'
    echo ----------------------------------------------------
  done
}

function pop() {
  OUTGOING_COUNT=$(git out | wc -l)
  if [[ $OUTGOING_COUNT -eq 0 ]]; then
    err "No outgoing changes, nothing to pop from"
  fi
  COMMIT_MESSAGE=$(get_last_commit_message | tr '\n' '$')
  git reset --soft HEAD^
  git stash save "${COMMIT_MESSAGE}" > /dev/null
}

function finish() {
  COMMIT_MESSAGE=$(git stash list | head -n 1 | cut -d : -f 3- | trim)
  git commit -a -m "$(echo ${COMMIT_MESSAGE} | tr '$' '\n')"
  git stash drop
}

function push() {
  INCOMMING_COUNT=$(list | wc -l)
  if [[ $INCOMMING_COUNT -eq 0 ]]; then
    err "No incomming changes, nothing to push from"
  fi
  if ! git stash apply --index; then
    git stash apply 
  fi
  finish
}

case $1 in
  pop)
    shift
    pop $*
    ;;
  push) 
    shift
    push $*
    ;;
  list)
    shift
    list $*
    ;;
  finish)
    shift
    finish $*
    ;;
  *)
    echo Wrong command: $1. Use: pop, push and list instead.
    ;;
esac
