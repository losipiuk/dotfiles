#!/bin/bash -e

function trim() {
  sed 's/^ *//g' | sed 's/ *$//g'
}

function err() {
  echo -e "$*"
  exit 1
}

function get_last_commit_message() {
  git log --max-count=1 --pretty=full | tail -n +5 | trim
}

