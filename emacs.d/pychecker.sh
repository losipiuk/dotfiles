#!/bin/bash
pyflakes $1
pep8 --max-line-length=120 $1
