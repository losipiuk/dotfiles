;; create load-path
(if (fboundp 'normal-top-level-add-subdirs-to-load-path)
    (normal-top-level-add-subdirs-to-load-path))

;; load required stuff
;(require 'emacs-goodies-el)

;; ************** cua ************************
(require 'cua-base)
(setq cua-enable-cua-keys nil)
(cua-mode 1)

;; python stuff

;; code checking via flymake
;; set code checker here from "epylint", "pyflakes"
(setq pycodechecker "~/.emacs.d/pychecker.sh")
(when (load "flymake" t)
  (defun flymake-pycodecheck-init ()
    (let* ((temp-file (flymake-init-create-temp-buffer-copy
                       'flymake-create-temp-inplace))
           (local-file (file-relative-name
                        temp-file
                        (file-name-directory buffer-file-name))))
      (list pycodechecker (list local-file))))
  (add-to-list 'flymake-allowed-file-name-masks
               '("\\.py\\'" flymake-pycodecheck-init)))

(require 'jedi)
(setq jedi:get-in-function-call-delay 100)
(add-hook 'python-mode-hook 
      (lambda () 
        (unless (eq buffer-file-name nil) (flymake-mode 1)) ;dont invoke flymake on temporary buffers for the interpreter
        (local-set-key [f2] 'flymake-goto-prev-error)
        (local-set-key [f3] 'flymake-goto-next-error)
	(local-set-key [f4] 'flymake-display-err-menu-for-current-line)
	(local-set-key (kbd "M-/") 'jedi:complete)
	(local-set-key (kbd "M-.") 'jedi:goto-definition)
	(local-set-key (kbd "M-,") 'jedi:goto-definition-pop-marker)
	(local-set-key (kbd ".") 'jedi:dot-complete)
	(jedi:setup)
        ))


(setq
 python-shell-interpreter "ipython"
 python-shell-interpreter-args ""
 python-shell-prompt-regexp "In \\[[0-9]+\\]: "
 python-shell-prompt-output-regexp "Out\\[[0-9]+\\]: "
 python-shell-completion-setup-code
   "from IPython.core.completerlib import module_completion"
 python-shell-completion-module-string-code
   "';'.join(module_completion('''%s'''))\n"
 python-shell-completion-string-code
   "';'.join(get_ipython().Completer.all_completions('''%s'''))\n")


;; melpa
;(add-to-list 'package-archives
;  '("melpa" . "http://melpa.milkbox.net/packages/") t)
;(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
