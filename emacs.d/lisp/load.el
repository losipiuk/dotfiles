
(setq elc-directory (concat "~/.emacs.d/lisp/el-" 
                            (car (split-string emacs-version "\\."))))
(setq elc-load-file (concat elc-directory "/load.el"))


 (if (file-exists-p elc-load-file)
    (progn
      (add-to-list 'load-path elc-directory)
      (setq prev-dir default-directory)
      (cd elc-directory)
      (load-file elc-load-file)
      (cd prev-dir)))

;(load-file "~/.emacs.d/lisp/my-color-theme.el")

(message elc-load-file)


;;; dodatkowe facy

(defvar font-lock-punctuation-face 'font-lock-punctuation-face
  "Don't even think of using this.")

(defface font-lock-punctuation-face '()
  "Font Lock mode face used for punctuation."
  :group 'font-lock-faces)


(defvar font-lock-number-face 'font-lock-number-face
  "Don't even think of using this.")


(defface font-lock-number-face '()
  "Font Lock mode face used for numbers."
  :group 'font-lock-faces)

(defvar font-lock-namespace-face 'font-lock-namespace-face
  "Don't even think of using this.")


(defface font-lock-namespace-face '()
  "Font Lock mode face used for namespaces."
  :group 'font-lock-faces)

;; kolorystyka

;;; reload the init file 
(defun reload ()                                         
  "Reload the .emacs file" 
  ( interactive "*" )
   (load-file "~/.emacs"))


(require 'fit-frame nil t) ;; (no error if not found): fit-frame
(require 'misc-fns nil t)  ;; (no error if not found): fontify-buffer
(require 'font-lock nil t) ;; (no error if not found): font-lock-fontify-buffer


;; ustawienia j�zykowe
(set-language-environment "UTF-8")

;; dired
(setq ls-lisp-dirs-first t)

(setq find-dired-simple-ignored-dirs '(".svn" "CVS"))

(defun flatten-lists (&rest list)
  "Return a new, flat list of composed all the remaining arguments and their sublists.
Dotted pairs are handled as lists.

  Examples:  (flatten-lists '(foo (bar baz) (quux (bletch (moby . bignum)))))
             => (foo bar baz quux bletch moby bignum)

             (flatten-lists '(a b (c d (e)) . f) '(1 2 ((3) . 4) 5))
             => (a b c d e f 1 2 3 4 5)"
  (cond ((consp (cdr list))
         (apply 'nconc (mapcar 'flatten-lists list)))
        ((consp (car list))
         (append (flatten-lists (car (car list)))
                 (flatten-lists (cdr (car list)))))
        ((car list)
         (list (car list)))))

(defalias 'flatten 'flatten-lists)

(defun find-dired-simple ()
  "Simplified version of find-dired.
   Asks for directory and for file pattern list.
   Ignores files which are in one of directories specified in find-dired-simple-ignored-dirs"
  (interactive)
  (let* ((directory (read-directory-name "base directory: "))
         (patterns-string (read-string "file pattens: " nil 'find-dired-simple-history))
         (patterns (split-string patterns-string))
         (find-arguments (flatten
          `(
           "-not"
           "("
           ,(split-string (mapconcat (lambda (p) (concat "-path */" p "/*")) find-dired-simple-ignored-dirs " -o "))
           ")"
           "-a"
           "-type"
           "f"
           "-a"
           "("
           ,(split-string (mapconcat (lambda (p) (concat "-name " p )) patterns " -o "))
           ")"
           )))
         (find-arguments-quoted (mapconcat 'shell-quote-argument find-arguments " "))
         )
    (find-dired directory find-arguments-quoted))) 

;; tuareg-mode
(setq auto-mode-alist (cons '("\\.ml\\w?" . tuareg-mode) auto-mode-alist))
(autoload 'tuareg-mode "tuareg" "Major mode for editing Caml code" t)

;; haskell-mode
(setq auto-mode-alist
      (append auto-mode-alist
              '(("\\.[hg]s$"  . haskell-mode)
                ("\\.hi$"     . haskell-mode)
                ("\\.l[hg]s$" . literate-haskell-mode))))
(autoload 'haskell-mode "haskell-mode"
  "Major mode for editing Haskell scripts." t)
(autoload 'literate-haskell-mode "haskell-mode"
  "Major mode for editing literate Haskell scripts." t)
(add-hook 'haskell-mode-hook 'turn-on-haskell-font-lock)
(add-hook 'haskell-mode-hook 'turn-on-haskell-decl-scan)
(add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)
(add-hook 'haskell-mode-hook 'turn-on-haskell-simple-indent)
(add-hook 'haskell-mode-hook 'turn-on-haskell-ghci)

;; php-mode
(defun custom-php-settings ()
  "Sets custom behavior for php mode"
  (interactive)
  (setq c-syntactic-indentation nil))
(add-hook 'php-mode-hook 'custom-php-settings)

(autoload 'php-mode "php-mode" "PHP editing mode" t)
(autoload 'two-mode-mode "two-mode-mode" "Mode for editing html - php" t)

(add-to-list 'auto-mode-alist '("\\.php[34]?\\'" . sgml-html-mode))
(add-to-list 'auto-mode-alist '("\\.php?\\'" . two-mode-mode))

;; html-helper 
;;(add-hook 'html-helper-load-hook '(lambda () (require 'html-font)))
(autoload 'html-helper-mode "html-helper-mode" "Yay HTML" t)
(setq auto-mode-alist (cons '("\\.html$" . html-helper-mode) auto-mode-alist))


;;  java-mode
(add-hook 'java-mode-hook '(lambda ()
                             (setq c-basic-offset 4)
                             (require 'hideshow)
                             (hs-minor-mode)
                             ))



;; lisp mode
(autoload 'cmulisp     "ilisp" "Inferior CMU Common Lisp." t)
(setq cmulisp-program "/usr/bin/lisp")
(setq allegro-program "~/bin/alisp")
(setq common-lisp-hyperspec-root "file:/usr/local/share/doc/lisp/HyperSpec/")
(setq common-lisp-hyperspec-symbol-table "/usr/local/share/doc/lisp/HyperSpec/Data/Map_Sym.txt")
(setq cltl2-root-url "file:/usr/local/share/doc/lisp/cltl2/")

(set-default 'auto-mode-alist
             (append '(("\\.lisp$" . lisp-mode)
                       ("\\.lsp$" . lisp-mode)
                       ("\\.cl$" . lisp-mode))
                     auto-mode-alist))
(add-hook 'lisp-mode-hook '(lambda () (require 'ilisp)))

(add-hook 'cmulisp-hook
          (lambda ()
            (setq ilisp-init-binary-extension "x86f")
            (setq ilisp-init-binary-command "(progn \"x86f\")")
            (setq ilisp-binary-extension "x86f")))

(add-hook 'ilisp-load-hook
          '(lambda ()
             ;; Change default key prefix to C-c

             ;; do not uncomment the line below - it fucks up prefix
             ;; (setq ilisp-*use-fsf-compliant-keybindings* t)

             (setq ilisp-*prefix* "\C-c")
             (setq ilisp-display-output-function #'ilisp-display-output-in-lisp-listener)


             ;; Set a keybinding for the COMMON-LISP-HYPERSPEC command
             (defkey-ilisp [(control c) h] 'common-lisp-hyperspec)

             ;; Make sure that you don't keep popping up the 'inferior
             ;; Lisp' buffer window when this is already visible in
             ;; another frame. Actually this variable has more impact
             ;; than that. Watch out.
                                        ; (setq pop-up-frames t)

             (message "Running ilisp-load-hook")
             ;; Define LispMachine-like key bindings, too.
                                        ; (ilisp-lispm-bindings) Sample initialization hook.

             ;; Set the inferior Lisp directory to the directory of
             ;; the buffer that spawned it on the first prompt.
             (add-hook 'ilisp-init-hook
                       '(lambda ()
                          (default-directory-lisp ilisp-last-buffer)))
             ))


;; c-mode
(defconst gadugadu-c-style
  '((c-offsets-alist           . ((substatement-open . 0) 
                                  (access-label . -) 
                                  (inclass . ++) 
                                  (case-label . +)))
    (c-basic-offset            . 2)
    ) "gadu-gadu c programming style")


(defun my-c-mode-font-lock ()
  (let* ((lowercase-iden-re "[[:lower:]_]+")
	 (namespace-re lowercase-iden-re))
    (font-lock-add-keywords nil
			    `(
			      ("^\\(#[[:alpha:]]+\\)\\>" (1 font-lock-preprocessor-face))
			      (,(concat "\\<\\(template\\|typename\\|union\\|struct\\|class\\|typedef\\|try\\|catch"
				       "\\|return\\|while\\|namespace\\|using\\|for\\|do\\|break\\|continue"
				       "\\|switch\\|if\\|else\\|case\\|goto\\|and\\|compl\\|export\\|or_eq"
				       "\\|xor\\|end_eq\\|extern\\|new\\|private\\|virtual\\|protected\\|public\\|xor_eq"
				       "\\|asm\\|const_cast\\|dynamic_cast\\|static_cast\\|reinterpret_cast\\|inline\\|not"
				       "\\|typeid\\|auto\\|not_eq\\|sizeof\\|volatile\\|mutable\\|bitand"
				       "\\|default\\|enum\\|register\\|static\\|throw\\|bitor\\|delete\\|explicit"
				       "\\|friend\\)\\>")
			       (1 font-lock-keyword-face))
			      
			      ,(list (concat 
				      "\\<\\(namespace\\)[[:space:]]+"
				      "\\(" namespace-re "\\)"
				      "\\>")
				     '(1 font-lock-keyword-face) 
				     '(2 font-lock-namespace-face))

			      ("\\<\\([[:lower:]][[:lower:][:digit:]_]+_t\\|const\\|void\\|float\\|double\\|bool\\|\\(\\(\\(?:un\\)?signed[[:space:]]+\\)?\\(short\\|int\\|long\\|char\\|long[[:space:]]+long\\)\\)\\)\\>" (1 font-lock-type-face))
			      ("\\<\\([[:upper:]]+[[:digit:][:upper:]]*[[:lower:]][[:alpha:][:digit:]_]+\\)\\>" (1 font-lock-type-face))
			      ("\\<\\(true\\|false\\)\\>" (1 font-lock-constant-face))
			      ("\\<\\([[:upper:]_]+\\)\\>" (1 font-lock-constant-face))
			      ("\\<\\([[:lower:]][[:alnum:]_]+\\)\\>" (1 font-lock-variable-name-face))
			      ("\\<\\(-?[[:digit:]]+\\([.][[:digit:]]+[flFL]?\\)?\\|[ulUL]?\\)\\>" (1 font-lock-number-face))
			      ("\\<\\(0x[[:digit:]abcdefABCDEF]+\\)\\>" (1 font-lock-number-face))
			      ("\\([&,;:|=.-+><*/^%!@(){}?]\\)" (1 font-lock-punctuation-face))
			    )
			  'set
                          )))

(defun my-c-mode-common-hook ()
  (c-add-style "gadugadu" gadugadu-c-style t)
  (setq outline-regexp "[\*{\*}]")
  (setq tab-width 2)
  (define-key c-mode-base-map "\C-cc" 'compile)
  (setq indent-tabs-mode nil)
  (c-set-style "gadugadu")
  (my-c-mode-font-lock)
  )

(add-hook 'c-mode-common-hook 'my-c-mode-common-hook)

;; emacs-lisp-mode
;(add-hook 'emacs-lisp-mode-hook 'my-lisp-mode-hook)

;; nxml-mode 
(add-to-list 'auto-mode-alist '("\\.xml\\'" . nxml-mode))
(setq nxml-child-indent 4)

;; version control
(defun log-edit-insert-files (file-names-fun)
  "Insert all files to be commited in log edit buffer. The arg
file-names-fun must be a function which returns list of file names to
insert."
  (let* ((files (funcall file-names-fun))
         (files-sorted (sort files #'string<)))
    (save-excursion
      (loop for f in files-sorted do
            (insert-string (format "* %s\n" f))))))

(add-hook 'log-edit-mode-hook
          (lambda () (local-set-key [(control c) (control i)]
                                    (lambda ()
                                      (interactive)
                                      (log-edit-insert-files
          #'log-edit-files)))))

;; hs-mode
(add-hook 'hs-minor-mode-hook '(lambda ()
                                 (local-set-key [C-kp-add] 'hs-show-block)
                                 (local-set-key [C-kp-subtract] 'hs-hide-block)))


;; show-paren-mode
(require 'paren)
(show-paren-mode)

;; tool-bar-mode
(require 'tool-bar)
(tool-bar-mode -1)

;; scroll-bar-mode
;;(scroll-bar-mode -1)

;; which-function-mode
(require 'which-func)
(which-function-mode t)

;; compilation-mode 
(setq compilation-window-height 10)
(defun custom-compilation-settings ()
  (add-to-list 'compilation-error-regexp-alist '("\\([^( \n\t]+\\)(\\([0-9]+\\),\\([0-9]+\\)) \\(Error\\|Fatal\\): " 1 2 3))
  (add-to-list 'compilation-error-regexp-alist '("^\\s-*\\[[^]]*\\]\\s-*\\(.+\\):\\([0-9]+\\):\\([0-9]+\\):[0-9]+:[0-9]+:" 1 2 3));ant + jikes
  (add-to-list 'compilation-error-regexp-alist '("^\\s-*\\[[^]]*\\]\\s-*\\(.+\\):\\([0-9]+\\):" 1 2))) ; ant + javac

(eval-after-load "compile" '(custom-compilation-settings))

(defun nop ()
  (interactive))

;; global-font-lock mode
(global-font-lock-mode 1)

;; ido-mode
(setq ido-enable-flex-matching t)
(setq ido-everywhere t)
(add-hook 'ido-setup-hook '(lambda () 
 			     (define-key ido-common-completion-map [up] 'next-history-element)
 			     (define-key ido-common-completion-map [down] 'previous-history-element)
 			     (define-key ido-file-dir-completion-map [up] 'next-history-element)
 			     (define-key ido-file-dir-completion-map [down] 'previous-history-element)
))
(ido-mode 1)

;;;; auto-complete mode
  ;; config fixes from http://curiousprogrammer.wordpress.com/2009/04/07/autocomplete-and-dabbrev/
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "/home/lucash/.emacs.d/lisp/el-23/dict")
(ac-config-default)

(require 'auto-complete)

;; manually enable autocompletion
(setq ac-auto-start nil)
(setq ac-quick-help-delay 0.1)
(global-set-key (kbd "M-/") 'ac-start)

  ;; some keybindings
(define-key ac-complete-mode-map (kbd "M-x") 'execute-extended-command)
(define-key ac-complete-mode-map (kbd "C-n") 'ac-next)
(define-key ac-complete-mode-map (kbd "C-p") 'ac-previous)
(define-key ac-complete-mode-map (kbd "C-g") 'ac-stop)
(define-key ac-complete-mode-map "\t" 'ac-complete)
(define-key ac-complete-mode-map "\r" 'ac-complete)

;; save-desktop
;; disable for now
(setq desktop-save nil)
(desktop-save-mode 1)
(setq desktop-globals-to-save
      (append '((extended-command-history . 30)
                (file-name-history        . 100)
                (grep-history             . 30)
                (compile-history          . 30)
                (minibuffer-history       . 50)
                (query-replace-history    . 60)
                (read-expression-history  . 60)
                (regexp-history           . 60)
                (regexp-search-ring       . 20)
                (search-ring              . 20)
                (shell-command-history    . 50)
                tags-file-name
                register-alist)))



;; og�lna konfiguracja
(setq inhibit-splash-screen t)
(setq visible-bell t)
(setq standard-indent 4)
(setq tab-width 3)
(setq tags-add-tables 'ask-user)
(setq tags-revert-without-query t)
(setq transient-mark-mode t)
(setq jit-lock-chunk-size 50000)
(setq jit-lock-stealth-time 0.1)
(setq fast-lock-minimum-size 100000)
(setq indent-tabs-mode nil)
(setq clip-large-size-font t)
(setq backup-directory-alist (list (cons "." "~/backupfiles")))
;(setq confirm-kill-emacs 'yes-or-no-p)
(setq grep-files-aliases 
      (quote 
       (("el" . "*.el") 
        ("c" . "*.c") 
        ("h" . "*.h") 
        ("asm" . "*.[sS]") 
        ("m" . "[Mm]akefile*") 
        ("l" . "[Cc]hange[Ll]og*") 
        ("tex" . "*.tex") 
        ("texi" . "*.texi") 
        ("ch" . "*.[ch] *.cc *.cpp *.hh *.hpp"))))
(setq x-enable-clipboard nil)

;; klawisze
(global-set-key "\C-x\C-b" 'buffer-menu)
(global-set-key "\C-x\C-g" 'goto-line)
(global-set-key "\C-cc" 'compile)
(define-key global-map [C-tab]  'other-window)
(define-key global-map [S-C-iso-lefttab]  'other-window-prev)

;; uruchamianmy wylaczone funkcje
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'dired-find-alternate-file 'disabled nil)


;; Mouse wheel support
(defun scroll-up-4 ()
  (interactive)
  (scroll-up 4))
(defun scroll-down-4 ()
  (interactive)
  (scroll-up -4))

(global-set-key [mouse-4] 'scroll-down-4)
(global-set-key [mouse-5] 'scroll-up-4)

(defun other-window-prev ()
  (interactive)
  (other-window -1)
)

(show-paren-mode 1)

;;; makra

(fset 'split-on-comma
   [?\C-s ?, ?\C-m return tab])


;;; macos stuff
(when (eq system-type 'darwin)
  (setq mac-option-modifier 'super)
  (setq mac-command-modifier 'meta))
