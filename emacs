(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(cua-mode t nil (cua-base))
 '(dabbrev-case-fold-search nil)
 '(icicle-reminder-prompt-flag 6)
 '(ido-default-buffer-method (quote selected-window))
 '(mh-identity-list
   (quote
    (("lukasz@osipiuk.net"
      (("From" . "lukasz@osipiuk.net"))))))
 '(semanticdb-default-save-directory "~/.semanticdb")
 '(semanticdb-ebrowse-file-match "\\.\\(hh?\\|HH?\\|hpp|h\\)")
 '(semanticdb-find-default-throttle (quote (project unloaded system)))
 '(semanticdb-persistent-path (quote ("~/.semanticdb")))
 '(show-paren-mode t))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "black" :foreground "white" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 130 :width normal :foundry "nil" :family "Helvetica")))))

;; load main file
(load-file "~/.emacs.d/lisp/load.el")
