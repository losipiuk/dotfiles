# ~/.bashrc: executed by bash(1) for non-login shells.
# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth
shopt -s histappend
HISTSIZE=50000
HISTFILESIZE=50000
HISTTIMEFORMAT='%Y-%m-%d %H:%M:%S '
PATH=$PATH:~/dotfiles_bin

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# setup prompt

noColor='\[\033[0m\]'
blackColor='\[\033[0;30m\]'
redColor='\[\033[0;31m\]'
greenColor='\[\033[0;32m\]'
yellowColor='\[\033[0;33m\]'
blueColor='\[\033[0;34m\]'
purpleColor='\[\033[0;35m\]'
cyanColor='\[\033[0;36m\]'
greyColor='\[\033[0;37m\]'

boldGrayColor='\[\033[1;30m\]'
boldRedColor='\[\033[1;31m\]'
boldGreenColor='\[\033[1;32m\]'
boldYellowColor='\[\033[1;33m\]'
boldBlueColor='\[\033[1;34m\]'
boldPurpleColor='\[\033[1;35m\]'
boldCyanColor='\[\033[1;36m\]'
boldWhiteColor='\[\033[1;37m\]'

hg_ps1_() {
    hg prompt "${cyanColor}{root|basename}${noColor}\
{ ${blueColor}{rev}${greyColor}}\
{(${cyanColor}{tip}${greyColor})${noColor}}\
{ ${greyColor}on:${cyanColor}{branch|quiet}${noColor}}\
{ ${greyColor}at:${noColor}{bookmark}${noColor}}\
{ ${greyColor}mq:${purpleColor}{patch|applied|quiet}${noColor}}{/${purpleColor}{patch|count|quiet}${noColor}}" 2> /dev/null

}

hg_ps1() {
    hg prompt "[{${greyColor}on:${cyanColor}{branch|quiet}${noColor} }\
{${greyColor}at:${noColor}{bookmark}${noColor} }\
{${greyColor}mq:${purpleColor}{patch|applied|quiet}${noColor}}{/${purpleColor}{patch|count|quiet}${noColor} }\
{${yellowColor}({patch})${noColor} }\
{${boldRedColor}{status|unknown}${noColor} }{${boldRedColor}{status|modified}${noColor} }\
] " 2> /dev/null

}

# Tell __git_ps1 to indicate the following states.
source /usr/local/etc/bash_completion.d/git-prompt.sh
export GIT_PS1_SHOWDIRTYSTATE=true
export GIT_PS1_SHOWUNTRACKEDFILES=true

function git_ps1() {
   echo '$(__git_ps1 "(%s) ")'
}

function pathComponents
{
    echo `python2 -c\
    "import string,re;   pth=re.sub('^$HOME','~',r\"$1\",1);   n = string.atoi(r\"$2\");   nc=r\"$3\";   sc=r\"$4\";\
    print nc + string.join(pth.split('/')[-n:], sc + '/' + nc);"`
}

function myPromptCommand {
    local exitCode=$?
    if [[ ${exitCode} == 0 ]];then
      exitCode=""
    else
      exitCode=" ($exitCode)"
    fi

    local virtualEnvName=`echo $VIRTUAL_ENV | rev | cut -d '/' -f 1 |rev`
    local newPWD=`pathComponents "\${PWD}" 4 \${blueColor} \${redColor}`                # the trimed colored path
    echo -ne "\033]0;${HOSTNAME%%.*}:${PWD/$HOME/~}\007"                                # this sets the title
    export PS1="${noColor}[${greenColor}\h${noColor}:${newPWD}${noColor}]${exitCode} [${virtualEnvName}] $(git_ps1)⌘ "
    history -a
}

PROMPT_COMMAND=myPromptCommand

#export PS1='$(hg_ps1)\n\u at \h in \w\n$ '
export PS1="${noColor}[${greenColor}\h${noColor}:${newPWD}${noColor}] $(hg_ps1)$(git_ps1)⌘ "
#export PS1="${noColor}[${greenColor}\h${noColor}:${newPWD}${noColor}] $(git_ps1)⌘ "

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

#jenv
#if which jenv > /dev/null; then
#  if [[ ! -e ~/.jenv_init ]];then
#    jenv init - >> ~/.jenv_init
#  fi
#  source ~/.jenv_init
#fi

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias h=hg
alias e="emacs -nw"
alias ec="emacsclient"
alias tmr="tmux rename-window"
alias tmre="tmux rename-window ''"

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  elif [ -f /usr/local/etc/bash_completion ]; then
    . /usr/local/etc/bash_completion
  fi
fi

export EDITOR=vim

trap 'prev_cmd=$this_cmd; this_cmd=$BASH_COMMAND' DEBUG

function inf {
  beep
  if [ $# -gt 0 ]; then 
    "$@"
  java -cp ~/bin Popup "$@"
  else 
    java -cp ~/bin Popup `history 1`
  fi  
}

bark() {
 ogg123 /usr/share/sounds/gnome/default/alerts/bark.ogg 2>/dev/null
}

glass() {
ogg123 /usr/share/sounds/gnome/default/alerts/glass.ogg 2>/dev/null
}

drip() {
 ogg123 /usr/share/sounds/gnome/default/alerts/drip.ogg 2>/dev/null
}

say() {
  espeak -v pl "$*"
} 

saye() {
  _LAST_ERRNO="$?"
  _LAST_CMD=$(echo $prev_cmd | cut -d ' ' -f 1)
  if [[ "$*" != "" ]];then
    _LAST_CMD="$*"
  fi

  if [[ $_LAST_ERRNO == "0" ]];then
    say "Dobrze: $_LAST_CMD"
  else
    say "Wyjebało się: $_LAST_CMD"
  fi
}

confirm () {
    # call with a prompt string or use a default
    read -r -p "${1:-Are you sure? [y/N]} " response
    case $response in
        [yY][eE][sS]|[yY]) 
            true
            ;;
        *)
            false
            ;;
    esac
}

alias intellij_fix='ibus-daemon -rd; sleep 1;  setxkbmap -layout pl'

# multiple monitors
alias laptop='xrandr --output LVDS1 --auto --output HDMI1 --off'
alias monitor='xrandr --output LVDS1 --off --output HDMI1 --auto'
alias dual='xrandr --output LVDS1 --auto --right-of HDMI1 --output HDMI1 --auto'
alias dualdock='xrandr --output HDMI3 --auto --right-of HDMI2 --output HDMI2 --auto --output HDMI1 --off --output LVDS1 --off'

# python
export PIP_REQUIRE_VIRTUALENV=true

# local stuff
if [[ -e ~/.bashrc_local ]];then
  source ~/.bashrc_local
fi

# mac bash completion
#if [ -f `brew --prefix`/etc/bash_completion ]; then
#      . `brew --prefix`/etc/bash_completion
#fi


# DOCKER cleanup commands
# Kill all running containers.
alias dockerkillall='docker kill $(docker ps -q)'
# Delete all stopped containers.
alias dockercleanc='printf "\n>>> Deleting stopped containers\n\n" && docker rm $(docker ps -a -q)'
# Delete all untagged images.
alias dockercleani='printf "\n>>> Deleting untagged images\n\n" && docker rmi $(docker images -q -f dangling=true)'
# Delete all stopped containers and untagged images.
alias dockerclean='dockercleanc || true && dockercleani'

# added by travis gem
[ -f /Users/losipiuk/.travis/travis.sh ] && source /Users/losipiuk/.travis/travis.sh

# disable session auto naming
DISABLE_AUTO_TITLE=true

alias rts='sed -e"s/[ \t]*$//" -i'

alias kill_idea='ps aux |grep Contents/MacOS/idea |grep -v grep |cut -b 10-23 |xargs kill -9'
